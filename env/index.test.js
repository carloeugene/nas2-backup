const env = require('./index')

describe('Environmen test', () => {
  it('should load test env', () => {
    expect(env.BASE_URL_API).toBe('http://10.100.20.75:8080')
    expect(env.BASE_SEARCH_URL_API).toBe('http://10.100.20.75:9200')
  })
})