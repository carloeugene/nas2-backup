const NODE_ENV = process.env.NODE_ENV || 'uat'
console.log(`Loading environment variables: ${NODE_ENV}`)

module.exports = require(`./${NODE_ENV}`)
