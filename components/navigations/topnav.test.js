jest.mock('sweetalert2')
import { shallowMount } from '@vue/test-utils'
import Component from './TopNav.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Top Menu', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs,
      propsData: {
        to: '/testRoute',
        name: 'Test Route'
      }
    })

    return wrapper
  }

  it('should be a Vue Component', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should set dashboard as active', () => {
    const wrapper = factory()
    wrapper.vm.setActive()
    const navLink = wrapper.find('.nav-link.active')
    expect(navLink.exists()).toBeTruthy()
  })

  it('should have "Test Route" title', () => {
    const wrapper = factory()
    wrapper.vm.setActive()
    const title = wrapper.find('.menu-title')
    expect(title.text()).toBe('Test Route')
  })
})
