jest.mock('sweetalert2')
import { shallowMount } from '@vue/test-utils'
import Component from './TopMenu.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Top Menu', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs
    })

    return wrapper
  }

  it('should be a Vue Component', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should set dashboard as active', () => {
    const wrapper = factory()
    router.push('/dashboard')
    wrapper.vm.setActive()
    const dashboard = wrapper.find('.dashboard-link.active')
    expect(dashboard.exists()).toBeTruthy()
  })
})
