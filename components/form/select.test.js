import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Component from './Select.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Select Component', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      propsData: {
        id: 'testid',
        label: 'Select',
        items: [{ name: 'item1', value: 'item1Val' }],
        name: 'select-test',
        value: 'select-value'
      }
    })

    return wrapper
  }
  it('should have "testName" label', () => {
    const wrapper = factory()
    const label = wrapper.find('label')
    expect(label.text()).toBe('select-test')
  })

  it('should have required indicator', () => {
    const wrapper = factory()
    wrapper.setProps({ required: true })

    const span = wrapper.find('.danger')
    expect(span.exists()).toBeTruthy()
  })

  it('should have "*" if required', () => {
    const wrapper = factory()
    wrapper.setProps({ required: true })

    const span = wrapper.find('.danger')
    expect(span.text()).toBe('*')
  })

  it('should have an id of "testid"', () => {
    const wrapper = factory()
    const select = wrapper.find('select')
    expect(select.attributes('id')).toBe('testid')
  })

  it('should have a name of "test-select"', () => {
    const wrapper = factory()
    const select = wrapper.find('select')
    expect(select.attributes('name')).toBe('select-test')
  })

  it('should have disable properties', () => {
    const wrapper = factory()
    wrapper.setProps({ disabled: true })

    const select = wrapper.find('select')
    expect(select.exists()).toBeTruthy()
  })

  it('should have isFilter properties when true', () => {
    const wrapper = factory()
    wrapper.setProps({ isFilter: true })

    const div = wrapper.find('div')
    expect(div.exists()).toBeTruthy()
  })

  it('shouldnt have isFilter properties when false', () => {
    const wrapper = factory()
    wrapper.setProps({ isFilter: false })

    const div = wrapper.find('div')
    expect(div.exists()).toBeTruthy()
  })

  it('should contain value for option', () => {
    const wrapper = factory()
    const optionValue = wrapper.find('option')
    expect(optionValue.text()).toBe('item1')
  })

  it('should contain value for selected option', () => {
    const wrapper = factory()
    const optionValue = wrapper.find('option')
    expect(optionValue.attributes('value')).toBe('item1Val')
  })

  it('should set value on radio button', () => {
    const wrapper = factory({ selected: false })
    expect(wrapper.find('input').exists()).toBeFalsy()
  })

  it('should check if the value is emitted', () => {
    const wrapper = factory()
    wrapper.vm.$emit('input', true)
    expect(wrapper.emitted().input).toBeTruthy()
  })
})
