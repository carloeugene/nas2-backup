import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Component from './AutoComplete.vue'
import { localVue, store, i18n } from '~/test-utils'

describe('Autocomplete Component', () => {
    function factory() {
      const wrapper = shallowMount(Component, {
        store,
        i18n,
        localVue,
        propsData: {
          id: 'testid',
          name: 'testname'
        }
      })
  
      return wrapper
    }

    it('should have "testName" label', () => {
        const wrapper = factory()
        const label = wrapper.find('label')
        expect(label.text()).toBe('testname')
      })

      it('should have required indicator', () => {
        const wrapper = factory()
        wrapper.setProps({ required: true })
    
        const span = wrapper.find('.danger')
        expect(span.exists()).toBeTruthy()
      })
    
      it('should have "*" if required', () => {
        const wrapper = factory()
        wrapper.setProps({ required: true })
    
        const span = wrapper.find('.danger')
        expect(span.text()).toBe('*')
      })
      
      it('should have disable properties', () => {
        const wrapper = factory()
        wrapper.setProps({ disabled: true })
    
        const input = wrapper.find('input')
        expect(input.exists()).toBeTruthy()
      })
    
      it('should have isFilter properties when true', () => {
        const wrapper = factory()
        wrapper.setProps({ isFilter: true })
    
        const div = wrapper.find('div')
        expect(div.exists()).toBeTruthy()
      })
    
      it('shouldnt have isFilter properties when false', () => {
        const wrapper = factory()
        wrapper.setProps({ isFilter: false })
    
        const div = wrapper.find('div')
        expect(div.exists()).toBeTruthy()
      })

      it('should check if the value is emitted', () => {
        const wrapper = factory()
        wrapper.vm.$emit('input', true)
        expect(wrapper.emitted().input).toBeTruthy()
      })
})
