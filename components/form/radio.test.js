import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Component from './Radio.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Radio button component', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      propsData: {
        id: 'testid',
        label: 'Radios',
        items: [{ name: 'item1', value: 'item1Val' }],
        name: 'radio-test'
      }
    })

    return wrapper
  }

  it('should have "testName" label', () => {
    const wrapper = factory()
    const label = wrapper.find('label')
    expect(label.text()).toBe('Radios')
  })

  it('should have list of radio buttons', () => {
    const wrapper = factory()

    const radio = wrapper.find('#radio-test_item1Val')
    expect(radio.attributes('value')).toBe('item1Val')
  })

  it('should have typeof item equals to string', () => {
    const wrapper = factory()
    wrapper.setProps({ items: ['item1', 'item2'] })
    const radio = wrapper.find('#radio-test_item1')
    expect(radio.attributes('value')).toBe('item1')
  })

  it('should return item name to be "item1"', () => {
    const wrapper = factory()
    const itemName = wrapper.find('div fieldset label')
    expect(itemName.text()).toBe('item1')
  })

  it('should have required indicator', () => {
    const wrapper = factory()
    wrapper.setProps({ required: true })

    const input = wrapper.find('input')
    expect(input.exists()).toBeTruthy()
  })

  it('should have "*" if required', () => {
    const wrapper = factory()
    wrapper.setProps({ required: true })

    const span = wrapper.find('.danger')
    expect(span.text()).toBe('*')
  })

  it('should set value on radio button', () => {
    const wrapper = factory({ checked: true })
    expect(wrapper.find('input').exists()).toBeTruthy()
  })

  it('should check if the value is emitted', () => {
    const wrapper = factory()
    wrapper.vm.$emit('input', true)
    expect(wrapper.emitted().input).toBeTruthy()
  })
})
