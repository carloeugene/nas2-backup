import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Component from './Input.vue'
import { localVue, store, i18n } from '~/test-utils'

describe('Input compnent', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      propsData: {
        id: 'testid',
        name: 'User Name'
      }
    })

    return wrapper
  }

  it('should have "testName" label', () => {
    const wrapper = factory()
    const label = wrapper.find('label')
    expect(label.text()).toBe('User Name')
  })

  it('should have id of "testid" in input', () => {
    const wrapper = factory()

    const input = wrapper.find('input')
    const span = wrapper.find('.danger')
    expect(input.attributes('id')).toBe('testid')
    expect(span.exists()).toBeFalsy()
  })

  it('should have required indicator', () => {
    const wrapper = factory()
    wrapper.setProps({ required: true })

    const span = wrapper.find('.danger')
    expect(span.exists()).toBeTruthy()
  })

  it('should check if the value is emitted', () => {
    const wrapper = factory()
    wrapper.vm.$emit('input', true)
    expect(wrapper.emitted().input).toBeTruthy()
  })
})
