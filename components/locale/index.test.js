import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import Component from './index.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('ComponentWithButtons', () => {
  it('commits a mutation when a button is clicked', () => {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue
    })

    wrapper.find('button').trigger('click')
    expect(wrapper).toBeTruthy()
  })
})
