import { createLocalVue, RouterLinkStub } from '@vue/test-utils'
import VueI18n from 'vue-i18n'
import Vuex from 'vuex'
import { state, mutations } from '~/store'
import VueRouter from 'vue-router'
import VueTestUtils from '@vue/test-utils'

VueTestUtils.config['$t'] = () => {}
VueTestUtils.config['$tc'] = () => {}

const localVue = createLocalVue()
localVue.use(VueI18n)
localVue.use(Vuex)
localVue.use(VueRouter)

const store = new Vuex.Store({
  state: state(),
  mutations
})

const i18n = new VueI18n({
  locale: store.state.locale,
  fallbackLocale: 'en',
  messages: {
    en: require('~/locales/en.json'),
    fr: require('~/locales/fr.json'),
    es: require('~/locales/es.json')
  }
})

const router = new VueRouter({
  routes: []
})

const stubs = {
  NuxtLink: RouterLinkStub
}

export { localVue, store, i18n, router, stubs }
