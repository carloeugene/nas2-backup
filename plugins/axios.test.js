import axios from './axios'

describe('Axios plugin', () => {
  const $axios = {
    onRequest(cb) {
      cb({ url: 'testUrl' })
    },
    onError(cb) {
      const error = {
        response: {
          status: '500'
        }
      }
      cb(error)
    }
  }
  it('should log on request base on config url', () => {
    const spy = jest.spyOn(global.console, 'log')

    axios({ $axios })
    expect(spy).toHaveBeenCalledTimes(2)
    expect(spy).toHaveBeenNthCalledWith(1, 'Making request to testUrl')
    expect(spy).toHaveBeenNthCalledWith(2, { error: 500 })
    spy.mockRestore()
  })
})
