const pkg = require('./package')
const envVars = require('./env')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'NAS Nationwide Appraisal Services',
    meta: [
      { content: 'text/html; charset=UTF-8', 'http-equiv': 'Content-Type' },
      { content: 'IE=edge', 'http-equiv': 'X-UA-Compatible' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui'
      },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      {
        rel: 'shortcut icon',
        type: 'image/x-icon',
        href: '/apple-icon-120.png'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700'
      },
      {
        rel: 'stylesheet',
        href:
          'https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css'
      }
    ],
    bodyAttrs: {
      class:
        'vertical-layout vertical-content-menu 2-columns   menu-expanded fixed-navbar',
      'data-open': 'click',
      'data-menu': 'vertical-content-menu',
      'data-col': '2-columns'
    },
    script: [
      // {
      //   src: 'https://www.recaptcha.net/recaptcha/api.js',
      //   async: true,
      //   defer: true
      // }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    // BEGIN VENDOR CSS
    '~/static/app-assets/css/vendors.css',
    '~/static/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css',
    '~/static/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css',
    '~/static/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css',
    // END VENDOR CSS
    // BEGIN MODERN CSS
    '~/static/app-assets/css/app.css',
    // END MODERN CSS
    // BEGIN Page Level CSS
    '~/static/app-assets/css/core/menu/menu-types/vertical-content-menu.css',
    '~/static/app-assets/css/core/colors/palette-gradient.css',
    '~/static/app-assets/fonts/feather/fonts/feather.ttf',
    // END Page Level CSS
    // BEGIN Custom Page Level CSS
    '~/static/assets/css/style.css'
    // END Custom Page Level CSS
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vuex-persist', ssr: false },
    '~/plugins/i18n.js',
    '~/plugins/axios',
    { src: '~plugins/vue-full-calendar', ssr: false }
  ],
  /*
  ** Sweet Alert 2 modules
  */
  // modules: [
  //   [
  //     'sweetalert2',
  //     {
  //       confirmButtonColor: '#41b882'
  //     }
  //   ]
  // ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
    // Doc: https://bootstrap-vue.js.org/docs/
    //'bootstrap-vue/nuxt'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxy: true
  },

  proxy: {
    '/api/': {
      target: envVars.BASE_URL_API,
      pathRewrite: { '^/api/': '' }
    },
    '/search-api/': {
      target: envVars.BASE_SEARCH_URL_API,
      pathRewrite: { '^/search-api/': '' }
    }
  },
  /*
  ** Build configuration
  */
  babel: {
    presets: ['es2015', 'stage-0'],
    plugins: ['transform-runtime']
  },
  // bootstrap-vue config
  // bootstrapVue: {
  //   bootstrapCSS: false, // or `css`
  //   bootstrapVueCSS: false // or `bvCSS`
  // },
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  env: envVars
}
