import { locales } from '~/lib/util/locale'
import { lists } from '~/lib/util/breadcrumbs'

export const state = () => ({
  locales,
  locale: 'en',
  lists,
  list: '/'
})

export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  },
  SET_LIST(state, list) {
    state.list = list
  }
}
