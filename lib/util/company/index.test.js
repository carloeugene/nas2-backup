import { getType, getTypes, getLenders, getLender, getSubprimes } from './index'

describe('Is types.js read on testing', () => {
  test('Is getTypes function exist? ', () => {
    expect(getTypes()).toBeTruthy
  })

  test('The size of type in types.js file is 2', () => {
    expect(getTypes().length).toEqual(2)
  })
})

describe('Is Lender.js read on testing', () => {
  test('Is getLenders function exist? ', () => {
    expect(getLenders()).toBeTruthy
  })

  test('The size of lenders on lender.js file is 2', () => {
    expect(getLenders().length).toEqual(2)
  })

  test('Yes value must be equal to Y', () => {
    const Y = getLender({ name: 'Yes' }, 'value')
    expect(Y).toBe('Y')
  })

  test('No value must be equal to N', () => {
    const N = getLender({ name: 'No' }, 'value')
    expect(N).toBe('N')
  })

  test('Yes should not be required', () => {
    const isRequired = getLender({ name: 'Yes' }, 'required')
    expect(isRequired).not.toBe(true)
  })

  test('No should not be required', () => {
    const isRequired = getLender({ name: 'No' }, 'required')
    expect(isRequired).not.toBe(true)
  })
})

describe('Is subprime.js read on testing', () => {
  test('The size of subprime on subprime.js file is 2', () => {
    expect(getSubprimes()).toBeTruthy
  })
  test('The size of lenders on lender.js file is 2', () => {
    expect(getSubprimes().length).toEqual(2)
  })
})

describe('Is getType function work properly', () => {
  test('Appraiser should be required', () => {
    const isRequired = getType({ name: 'Appraiser' }, 'required')
    expect(isRequired).toBe(true)
  })

  test('Client should be required', () => {
    const isRequired = getType({ name: 'Client' }, 'required')
    expect(isRequired).toBe(true)
  })

  test('Apraiser value must be equal to A', () => {
    const A = getType({ name: 'Appraiser' }, 'value')
    expect(A).toBe('A')
  })

  test('Client value must be equal to C', () => {
    const C = getType({ name: 'Client' }, 'value')
    expect(C).toBe('C')
  })

  test('is get type name', () => {
    const A = getType({ name: 'Appraiser' })
    expect(A.name).toBe('Appraiser')

    const C = getType({ name: 'Client' })
    expect(C.name).toBe('Client')
  })

  test('is get type value', () => {
    const A = getType({ value: 'C' })
    expect(A.value).toBe('C')

    const C = getType({ value: 'C' })
    expect(C.value).toBe('C')
  })

  test('is get lender name', () => {
    const Y = getLender({ name: 'Yes' })
    expect(Y.value).toBe('Y')

    const N = getLender({ name: 'No' })
    expect(N.value).toBe('N')
  })
})
