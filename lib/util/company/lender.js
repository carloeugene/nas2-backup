export default [
  { name: 'Yes', value: 'Y', required: false },
  { name: 'No', value: 'N', required: false }
]
