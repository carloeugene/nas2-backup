import _ from 'lodash'
import types from './types'
import lender from './lender'
import subprime from './subprime'

function getTypes() {
  return _.slice(types)
}

function getLenders() {
  return _.slice(lender)
}
function getSubprimes() {
  return _.slice(subprime)
}

/**
 * Find type by query
 *
 * @param {*} query sample query = {value: 'X'}
 * @param {string} field - optional. return one field
 */
function getType(query, field) {
  const foundType = _.find(types, query)

  if (field) {
    return _.get(foundType, field, '')
  }

  return foundType
}

/**
 * Find type by query
 *
 * @param {*} query sample query = {value: 'Y'}
 * @param {string} field - optional. return one field
 */
function getLender(query, field) {
  const foundLender = _.find(lender, query)

  if (field) {
    return _.get(foundLender, field, '')
  }

  return foundLender
}

export { getTypes, getType, getLenders, getLender, getSubprimes }
