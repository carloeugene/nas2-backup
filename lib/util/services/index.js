import _ from 'lodash'
import category from './category'

function getCategory(query, field) {
  const foundCategory = _.find(category, query)

  if (field) {
    return _.get(foundCategory, field, '')
  }

  return foundCategory
}

function getCategories() {
  return _.slice(category)
}

export { getCategory, getCategories }
