import { getCategory, getCategories } from './index'

test('C is equivalent to Commercial', () => {
  const C = getCategory({ name: 'Commercial' }, 'value')
  expect(C).toBe('C')
  const Cname = getCategory({ value: 'C' })
  expect(Cname.name).toBe('Commercial')
})

test('R is equivalent to Residential', () => {
  const R = getCategory({ name: 'Residential' }, 'value')
  expect(R).toBe('R')
  const Rname = getCategory({ value: 'R' })
  expect(Rname.name).toBe('Residential')
})

test('Category size must be 2', () => {
  expect(getCategories().length).toEqual(2)
})
