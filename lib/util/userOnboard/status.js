export default [
  { name: 'New', value: 'N' },
  { name: 'Active', value: 'A' },
  { name: 'Disable', value: 'D' }
]
