import _ from 'lodash'
import status from './status'
import role from './role'

function getStatus(query, field) {
  const foundStatus = _.find(status, query)

  if (field) {
    return _.get(foundStatus, field, '')
  }

  return foundStatus
}

function getStatuses() {
  return _.slice(status)
}

function getRole(query, field) {
  const foundRole = _.find(role, query)
  if (field) {
    return _.get(foundRole, field, '')
  }
}
function getRoles() {
  return _.slice(role)
}
export { getStatus, getStatuses, getRole, getRoles }
