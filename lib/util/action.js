/**
 * render element guide https://alligator.io/vuejs/introduction-render-functions/
 * https://vuejs.org/v2/guide/components-dynamic-async.html#ad
 *
 * For now I think this is the way to render dynamic components in Vue js
 * This is equivalent to
 * <div>
 *  <nuxt-link to="/company/1"><i class="ft-edit /></nuxt>
 *  <button type= 'button' "><i class="ft-trash /></button>
 * </div>
 */
function getAction(data, hide, path) {
  return {
    render(createElement) {
      // div first element
      return createElement('div', [
        createElement(
          'nuxt-link', // then will create the first nuxt-link element
          {
            attrs: {
              to: `${path}${data}` // with to attribute /region/1
            }
          },
          // inside the first element is the icon for the nuxt-link
          [
            createElement('i', {
              class: 'ft-edit black'
            })
          ]
        ),
        // repeat the above example
        createElement(
          'button',
          {
            attrs: {
              type: 'button',
              class: 'btn btn-md',
              style: 'background-color:transparent'
            },
            on: {
              click(e) {
                hide(data)
              }
            }
          },
          [
            createElement('i', {
              class: 'ft-trash-2' // but now it used the trash icon
            })
          ]
        )
      ])
    }
  }
}

export { getAction }
