import { getDisplay, locales, mapping } from './index'

describe('All the languanges is English, Français and Español', () => {
  test('Mapping en is equivalent to English', () => {
    const locale = mapping.en
    expect(locale).toBe('English')
  })
  test('Mapping fr is equivalent to Français', () => {
    const locale = mapping.fr
    expect(locale).toBe('Français')
  })
  test('Mapping es is equivalent to Español', () => {
    const locale = mapping.es
    expect(locale).toBe('Español')
  })
})

describe('Language prefix used are en, fr, es', () => {
  test('locales must be en', () => {
    expect(locales[0]).toBe('en')
  })
  test('locales must be fr', () => {
    expect(locales[1]).toBe('fr')
  })
  test('locales must be es', () => {
    expect(locales[2]).toBe('es')
  })
})

describe('Is getDisplay a function', () => {
  test('en display must be equal to English ', () => {
    const en = getDisplay('en')
    expect(en).toBe('English')
  })
  test('fr display must be equal to Français ', () => {
    const fr = getDisplay('fr')
    expect(fr).toBe('Français')
  })
  test('es display must be equal to Español ', () => {
    const es = getDisplay('es')
    expect(es).toBe('Español')
  })
})
