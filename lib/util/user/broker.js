export default [
  { name: 'Axiom', value: 'Axiom' },
  { name: 'Centum', value: 'Centum' },
  { name: 'CML', value: 'CML' },
  { name: 'DLC', value: 'DLC' },
  { name: 'INVIS/Mortgage Intelligence', value: 'INVIS/Mortgage Intelligence' },
  { name: 'Mortgage Architects', value: 'Mortgage Architects' },
  { name: 'Mortgage Centre', value: 'Mortgage Centre' },
  { name: 'Oriana', value: 'Oriana' },
  { name: 'Test', value: 'Test' },
  { name: 'TMACC', value: 'TMACC' },
  { name: 'TMG', value: 'TMG' },
  { name: 'Verico', value: 'Verico' }
]
