export default [
  { name: 'Airtel', value: 'Airtel' },
  { name: 'Bell/Solo', value: 'Bell/Solo' },
  { name: 'Fido', value: 'Fido' },
  { name: 'Koodo', value: 'Koodo' },
  { name: 'Telus', value: 'Telus' },
  { name: 'MTS', value: 'MTS' },
  { name: 'PC mobile', value: 'PC mobile' },
  { name: 'Petro Canada', value: 'Petro Canada' },
  { name: 'Public Mobile', value: 'Public Mobile' },
  { name: 'Rogers', value: 'Rogers' },
  { name: 'Sasktel', value: 'Sasktel' },
  { name: 'SpeakOut', value: 'SpeakOut' },
  { name: 'Telus', value: 'Telus' },
  { name: 'Virgin', value: 'Virgin' },
  { name: 'Wind', value: 'Wind' }
]
