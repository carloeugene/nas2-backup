import {
  getMqUsers,
  getTypes,
  getType,
  getAppTypes,
  getBrokerHouse,
  getProvider
} from './index'

describe('Is getType funtion works', () => {
  test('Appraiser is equivalent to A', () => {
    const A = getType({ name: 'Appraiser' }, 'value')
    expect(A).toBe('A')

    const type = getType({ value: 'A' })
    expect(type.name).toBe('Appraiser')
  })
  test('Broker is equivalent to B', () => {
    const B = getType({ name: 'Broker' }, 'value')
    expect(B).toBe('B')

    const type = getType({ value: 'B' })
    expect(type.name).toBe('Broker')
  })
  test('Client is equivalent to C', () => {
    const C = getType({ name: 'Client' }, 'value')
    expect(C).toBe('C')

    const type = getType({ value: 'C' })
    expect(type.name).toBe('Client')
  })
  test('System Admin is equivalent to S', () => {
    const S = getType({ name: 'System Admin' }, 'value')
    expect(S).toBe('S')

    const type = getType({ value: 'S' })
    expect(type.name).toBe('System Admin')
  })
})

test('getType size must be equal to 4', () => {
  expect(getTypes().length).toEqual(4)
})

describe('AppType name must be same as the AppType value', () => {
  for (let i = 0; i < getAppTypes().length; i++) {
    const AppName = getAppTypes()[i].name
    const AppValue = getAppTypes()[i].value

    test(`${AppName} name is not equal to ${AppValue} value`, () => {
      const result = getAppTypes()[i]
      expect(result.name).toBe(AppValue)
    })
  }
})

describe('Broker name must be same as the Broker value', () => {
  for (let i = 0; i < getBrokerHouse().length; i++) {
    const brokerName = getBrokerHouse()[i].name
    const brokerValue = getBrokerHouse()[i].value

    test(`${brokerName} name is not equal to ${brokerValue} value`, () => {
      const result = getBrokerHouse()[i]
      expect(result.name).toBe(brokerValue)
    })
  }
})

describe('Provider name must be same as the Provider value', () => {
  for (let i = 0; i < getProvider().length; i++) {
    const providerName = getProvider()[i].name
    const providerValue = getProvider()[i].value

    test(`${providerName} name is not equal to ${providerValue} value`, () => {
      const result = getProvider()[i]
      expect(result.name).toBe(providerValue)
    })
  }
})

describe('MQUser name must be same as the MQUser value', () => {
  for (let i = 0; i < getMqUsers().length; i++) {
    const MQUserName = getMqUsers()[i].name
    const MQUserValue = getMqUsers()[i].value

    test(`${MQUserName} name is not equal to ${MQUserValue} value`, () => {
      const result = getMqUsers()[i]
      expect(result.name).toBe(MQUserName)
    })
  }
})
