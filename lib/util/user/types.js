export default [
  { name: 'Appraiser', value: 'A' },
  { name: 'Broker', value: 'B' },
  { name: 'Client', value: 'C' },
  { name: 'System Admin', value: 'S' }
]
