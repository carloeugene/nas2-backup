import mqUser from './mqUser.js'
import type from './types.js'
import appType from './appType.js'
import broker from './broker.js'
import provider from './provider.js'

import _ from 'lodash'

function getMqUsers() {
  return _.slice(mqUser)
}

function getTypes() {
  return _.slice(type)
}

function getAppTypes() {
  return _.slice(appType)
}

function getBrokerHouse() {
  return _.slice(broker)
}

function getProvider() {
  return _.slice(provider)
}

/**
 * Find type by query
 *
 * @param {*} query sample query = {value: 'A'}
 * @param {string} field - optional. return one field
 */
function getType(query, field) {
  const foundType = _.find(type, query)

  if (field) {
    return _.get(foundType, field, '')
  }

  return foundType
}

export {
  getMqUsers,
  getTypes,
  getType,
  getAppTypes,
  getBrokerHouse,
  getProvider
}
