import _ from 'lodash'
import status from './status'
import priceType from './priceType'

function getStatus(query, field) {
  const foundStatus = _.find(status, query)

  if (field) {
    return _.get(foundStatus, field, '')
  }

  return foundStatus
}

function getStatuses() {
  return _.slice(status)
}

function getPriceTypes() {
  return _.slice(priceType)
}

function getPriceType(query, field) {
  const foundPriceType = _.find(priceType, query)

  if (field) {
    return _.get(foundPriceType, field, '')
  }

  return foundPriceType
}

export { getStatus, getStatuses, getPriceType, getPriceTypes }
