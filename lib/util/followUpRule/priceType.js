export default [
  { name: 'Listing', value: 'Listing' },
  { name: 'Market Value', value: 'Market Value' },
  { name: 'Port', value: 'Port' },
  { name: 'Power of Sale', value: 'Power of Sale' },
  { name: 'Purchase plus Improvement', value: 'Purchase plus Improvement' },
  { name: 'Purchase Price', value: 'Purchase Price' },
  { name: 'Refinance Amount', value: 'Refinance Amount' },
  { name: 'Renewal', value: 'Renewal' },
  { name: 'Transfer/Switch', value: 'Transfer/Switch' }
]
