import { getStatus, getStatuses, getPriceType, getPriceTypes } from './index'

describe('Is getStatus a function', () => {
  for (let i = 0; i < getStatuses().length; i++) {
    it('is status name', () => {
      const statusName = getStatuses()[i].name
      const statusValue = getStatuses()[i].value
      const N = getStatus({ name: statusName })
      expect(N.name).toBe(statusValue)
    })
  }

  for (let i = 0; i < getStatuses().length; i++) {
    it('is status value', () => {
      const statusName = getStatuses()[i].name
      const statusValue = getStatuses()[i].value
      const N = getStatus({ value: statusValue })
      expect(N.value).toBe(statusName)
    })
  }

  for (let i = 0; i < getStatuses().length; i++) {
    test('status name must be same as the status value', () => {
      const statusName = getStatuses()[i].name
      const result = getStatus({ name: statusName }, 'value')
      expect(result).toBe(statusName)
    })
  }

  test('Length of status array must be 21', () => {
    expect(getStatuses().length).toBe(21)
  })
})

describe('Is getPriceType a function', () => {
  test('Length of price type array must be 9', () => {
    expect(getPriceTypes().length).toBe(9)
  })

  for (let i = 0; i < getPriceTypes().length; i++) {
    test('price type name must be same as the price type value', () => {
      const priceName = getPriceTypes()[i].name
      const result = getPriceType({ name: priceName }, 'value')
      expect(result).toBe(priceName)
    })
  }

  for (let i = 0; i < getPriceTypes().length; i++) {
    it('is price type name', () => {
      const priceName = getPriceTypes()[i].name
      const priceValue = getPriceTypes()[i].value
      const N = getPriceType({ name: priceName })
      expect(N.name).toBe(priceValue)
    })
  }

  for (let i = 0; i < getPriceTypes().length; i++) {
    it('is price type value', () => {
      const priceName = getPriceTypes()[i].name
      const priceValue = getPriceTypes()[i].value
      const N = getPriceType({ name: priceValue })
      expect(N.name).toBe(priceName)
    })
  }
})
