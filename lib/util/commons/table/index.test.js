import { getYN, getYNs } from './index'

test('Get Yes No on Common table', () => {
  const Y = getYN({ name: 'Yes' })
  expect(Y.value).toBe('Y')

  const Yes = getYN({ value: 'Y' }, 'name')
  expect(Yes).toBe('Yes')

  const N = getYN({ name: 'No' })
  expect(N.value).toBe('N')

  const No = getYN({ value: 'N' }, 'name')
  expect(No).toBe('No')
})

test('Get Y and N size on common table', () => {
  const arrayLength = getYNs()
  expect(arrayLength.length).toEqual(2)
})
