import _ from 'lodash'
import yN from './yN'

function getYN(query, field) {
  const foundYN = _.find(yN, query)

  if (field) {
    return _.get(foundYN, field, '')
  }

  return foundYN
}

function getYNs() {
  return _.slice(yN)
}

export { getYN, getYNs }
