export default [
  { name: 'Canada', value: 'CAN' },
  { name: 'Mexico', value: 'MEX' },
  { name: 'United States of America', value: 'USA' }
]
