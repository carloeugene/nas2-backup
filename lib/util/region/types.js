export default [
  { name: 'Remote', value: 'X' },
  { name: 'Rural', value: 'R' },
  { name: 'Semi-Urban', value: 'S' },
  { name: 'Urban', value: 'U' }
]
