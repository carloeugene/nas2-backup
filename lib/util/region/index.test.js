import {
  getProvinces,
  getProvince,
  getCountries,
  getCountry,
  getTypes,
  getType
} from './index'

describe('Is getCountry a funtion', () => {
  test('CAN is equivalent to Canada', () => {
    const country = getCountry({ value: 'CAN' })
    expect(country.name).toBe('Canada')

    const code = getCountry({ name: 'Canada' })
    expect(code.value).toBe('CAN')

    const getValue = getCountry({ name: 'Canada' }, 'value')
    expect(getValue).toBe('CAN')
  })
  test('MEX is equivalent to Mexico', () => {
    const country = getCountry({ value: 'MEX' })
    expect(country.name).toBe('Mexico')

    const code = getCountry({ name: 'Mexico' })
    expect(code.value).toBe('MEX')

    const getValue = getCountry({ name: 'Mexico' }, 'value')
    expect(getValue).toBe('MEX')
  })
  test('USA is equivalent to United State of America', () => {
    const country = getCountry({ value: 'USA' })
    expect(country.name).toBe('United States of America')

    const code = getCountry({ name: 'United States of America' })
    expect(code.value).toBe('USA')

    const getValue = getCountry({ name: 'United States of America' }, 'value')
    expect(getValue).toBe('USA')
  })
})

describe('Is getTypes a function', () => {
  test('Remote is equivalent to X', () => {
    const type = getType({ value: 'X' }, 'name')
    expect(type).toBe('Remote')
    const N = getType({ name: 'Remote' }, 'value')
    expect(N).toBe('X')

    const Rtype = getType({ value: 'X' })
    expect(Rtype.name).toBe('Remote')
    const R = getType({ name: 'Remote' })
    expect(R.value).toBe('X')
  })

  test('Rural is equivalent to R', () => {
    const type = getType({ value: 'R' }, 'name')
    expect(type).toBe('Rural')
    const N = getType({ name: 'Rural' }, 'value')
    expect(N).toBe('R')

    const Rtype = getType({ value: 'R' })
    expect(Rtype.name).toBe('Rural')
    const R = getType({ name: 'Rural' })
    expect(R.value).toBe('R')
  })

  test('Semi-Urban is equivalent to S', () => {
    const type = getType({ value: 'S' }, 'name')
    expect(type).toBe('Semi-Urban')
    const N = getType({ name: 'Semi-Urban' }, 'value')
    expect(N).toBe('S')

    const Stype = getType({ value: 'S' })
    expect(Stype.name).toBe('Semi-Urban')
    const S = getType({ name: 'Semi-Urban' })
    expect(S.value).toBe('S')
  })

  test('Urban is equivalent to U', () => {
    const type = getType({ value: 'U' }, 'name')
    expect(type).toBe('Urban')
    const N = getType({ name: 'Urban' }, 'value')
    expect(N).toBe('U')

    const Utype = getType({ value: 'U' })
    expect(Utype.name).toBe('Urban')
    const U = getType({ name: 'Urban' })
    expect(U.value).toBe('U')
  })
})

describe('Is getProvince for Canada works', () => {
  for (let i = 0; i < getProvinces('CAN').length; i++) {
    const province = getProvinces('CAN')
    test(`${province[i].name} is equivalent to ${province[i].value}`, () => {
      const result = getProvince('CAN', { value: province[i].value })
      expect(result.name).toBe(province[i].name)
    })
  }
  test('Alberta is equivalent to AB', () => {
    const AB = getProvince('CAN', { value: 'AB' }, 'name')
    expect(AB).toBe('Alberta')
  })
  test('British Columbia is equivalent to BC', () => {
    const BC = getProvince('CAN', { value: 'BC' }, 'name')
    expect(BC).toBe('British Columbia')
  })
  test('Manitoba is equivalent to MB', () => {
    const MB = getProvince('CAN', { value: 'MB' }, 'name')
    expect(MB).toBe('Manitoba')
  })
  test('New Brunswick is equivalent to NB', () => {
    const NB = getProvince('CAN', { value: 'NB' }, 'name')
    expect(NB).toBe('New Brunswick')
  })
  test('North West Territories is equivalent to NT', () => {
    const NT = getProvince('CAN', { value: 'NT' }, 'name')
    expect(NT).toBe('North West Territories')
  })
  test('Nova Scotia is equivalent to NS', () => {
    const NS = getProvince('CAN', { value: 'NS' }, 'name')
    expect(NS).toBe('Nova Scotia')
  })
  test('Nunavut is equivalent to NU', () => {
    const NU = getProvince('CAN', { value: 'NU' }, 'name')
    expect(NU).toBe('Nunavut')
  })
  test('Ontario is equivalent to ON', () => {
    const ON = getProvince('CAN', { value: 'ON' }, 'name')
    expect(ON).toBe('Ontario')
  })
  test('P.E.I is equivalent to PE', () => {
    const PE = getProvince('CAN', { value: 'PE' }, 'name')
    expect(PE).toBe('P.E.I')
  })
  test('Quebec is equivalent to PE', () => {
    const QC = getProvince('CAN', { value: 'QC' }, 'name')
    expect(QC).toBe('Quebec')
  })
  test('Saskatchewan is equivalent to SK', () => {
    const SK = getProvince('CAN', { value: 'SK' }, 'name')
    expect(SK).toBe('Saskatchewan')
  })
  test('Yukon Territories is equivalent to YT', () => {
    const YT = getProvince('CAN', { value: 'YT' }, 'name')
    expect(YT).toBe('Yukon Territories')
  })
})

describe('Is getProvince for USA works', () => {
  for (let i = 0; i < getProvinces('USA').length; i++) {
    const province = getProvinces('USA')
    test(`${province[i].name} is equivalent to ${province[i].value}`, () => {
      const result = getProvince('USA', { value: province[i].value })
      expect(result.name).toBe(province[i].name)
    })
  }
  test('Alberta is equivalent to AB', () => {
    const AB = getProvince('USA', { value: 'AB' }, 'name')
    expect(AB).toBe('Alberta')
  })
  test('British Columbia is equivalent to BC', () => {
    const BC = getProvince('USA', { value: 'BC' }, 'name')
    expect(BC).toBe('British Columbia')
  })
  test('Manitoba is equivalent to MB', () => {
    const MB = getProvince('USA', { value: 'MB' }, 'name')
    expect(MB).toBe('Manitoba')
  })
  test('New Brunswick is equivalent to NB', () => {
    const NB = getProvince('USA', { value: 'NB' }, 'name')
    expect(NB).toBe('New Brunswick')
  })
  test('North West Territories is equivalent to NT', () => {
    const NT = getProvince('USA', { value: 'NT' }, 'name')
    expect(NT).toBe('North West Territories')
  })
  test('Nova Scotia is equivalent to NS', () => {
    const NS = getProvince('USA', { value: 'NS' }, 'name')
    expect(NS).toBe('Nova Scotia')
  })
  test('Nunavut is equivalent to NU', () => {
    const NU = getProvince('USA', { value: 'NU' }, 'name')
    expect(NU).toBe('Nunavut')
  })
  test('Ontario is equivalent to ON', () => {
    const ON = getProvince('USA', { value: 'ON' }, 'name')
    expect(ON).toBe('Ontario')
  })
  test('P.E.I is equivalent to PE', () => {
    const PE = getProvince('USA', { value: 'PE' }, 'name')
    expect(PE).toBe('P.E.I')
  })
  test('Quebec is equivalent to PE', () => {
    const QC = getProvince('USA', { value: 'QC' }, 'name')
    expect(QC).toBe('Quebec')
  })
  test('Saskatchewan is equivalent to SK', () => {
    const SK = getProvince('USA', { value: 'SK' }, 'name')
    expect(SK).toBe('Saskatchewan')
  })
  test('Yukon Territories is equivalent to YT', () => {
    const YT = getProvince('USA', { value: 'YT' }, 'name')
    expect(YT).toBe('Yukon Territories')
  })
})

describe('Is getProvince for Mexico works', () => {
  for (let i = 0; i < getProvinces('MEX').length; i++) {
    const province = getProvinces('MEX')
    test(`${province[i].name} is equivalent to ${province[i].value}`, () => {
      const result = getProvince('MEX', { value: province[i].value })
      expect(result.name).toBe(province[i].name)
    })
  }
  test('Aguascalientes is equivalent to AG', () => {
    const AG = getProvince('MEX', { value: 'AG' }, 'name')
    expect(AG).toBe('Aguascalientes')
  })
  test('Baja California is equivalent to BN', () => {
    const BN = getProvince('MEX', { value: 'BN' }, 'name')
    expect(BN).toBe('Baja California')
  })
  test('Baja California Sur is equivalent to BS', () => {
    const BS = getProvince('MEX', { value: 'BS' }, 'name')
    expect(BS).toBe('Baja California Sur')
  })
  test('Campeche is equivalent to CM', () => {
    const CM = getProvince('MEX', { value: 'CM' }, 'name')
    expect(CM).toBe('Campeche')
  })
  test('Chiapas is equivalent to CP', () => {
    const CP = getProvince('MEX', { value: 'CP' }, 'name')
    expect(CP).toBe('Chiapas')
  })
  test('Chihuahua is equivalent to CH', () => {
    const CH = getProvince('MEX', { value: 'CH' }, 'name')
    expect(CH).toBe('Chihuahua')
  })
  test('Coahuila is equivalent to CA', () => {
    const CA = getProvince('MEX', { value: 'CA' }, 'name')
    expect(CA).toBe('Coahuila')
  })
  test('Colima is equivalent to CL', () => {
    const CL = getProvince('MEX', { value: 'CL' }, 'name')
    expect(CL).toBe('Colima')
  })
  test('Durango is equivalent to DU', () => {
    const DU = getProvince('MEX', { value: 'DU' }, 'name')
    expect(DU).toBe('Durango')
  })
  test('Federal District is equivalent to DF', () => {
    const DF = getProvince('MEX', { value: 'DF' }, 'name')
    expect(DF).toBe('Federal District')
  })
  test('Guanajuato is equivalent to GT', () => {
    const GT = getProvince('MEX', { value: 'GT' }, 'name')
    expect(GT).toBe('Guanajuato')
  })
  test('Guerrero is equivalent to GR', () => {
    const GR = getProvince('MEX', { value: 'GR' }, 'name')
    expect(GR).toBe('Guerrero')
  })
  test('Hidalgo is equivalent to HI', () => {
    const HI = getProvince('MEX', { value: 'HI' }, 'name')
    expect(HI).toBe('Hidalgo')
  })
  test('Jalisco is equivalent to JA', () => {
    const JA = getProvince('MEX', { value: 'JA' }, 'name')
    expect(JA).toBe('Jalisco')
  })
  test('Mexico State is equivalent to MX', () => {
    const MX = getProvince('MEX', { value: 'MX' }, 'name')
    expect(MX).toBe('Mexico State')
  })
  test('Michoacán is equivalent to MC', () => {
    const MC = getProvince('MEX', { value: 'MC' }, 'name')
    expect(MC).toBe('Michoacán')
  })
  test('Morelos is equivalent to MR', () => {
    const MR = getProvince('MEX', { value: 'MR' }, 'name')
    expect(MR).toBe('Morelos')
  })
  test('Nayarit is equivalent to NA', () => {
    const NA = getProvince('MEX', { value: 'NA' }, 'name')
    expect(NA).toBe('Nayarit')
  })
  test('Nuevo León is equivalent to NL', () => {
    const NL = getProvince('MEX', { value: 'NL' }, 'name')
    expect(NL).toBe('Nuevo León')
  })
  test('Oaxaca is equivalent to OA', () => {
    const OA = getProvince('MEX', { value: 'OA' }, 'name')
    expect(OA).toBe('Oaxaca')
  })
  test('Puebla is equivalent to PU', () => {
    const PU = getProvince('MEX', { value: 'PU' }, 'name')
    expect(PU).toBe('Puebla')
  })
  test('Querétaro is equivalent to QE', () => {
    const QE = getProvince('MEX', { value: 'QE' }, 'name')
    expect(QE).toBe('Querétaro')
  })
  test('Quintana Roo is equivalent to QR', () => {
    const QR = getProvince('MEX', { value: 'QR' }, 'name')
    expect(QR).toBe('Quintana Roo')
  })
  test('San Luis Potosí is equivalent to SL', () => {
    const SL = getProvince('MEX', { value: 'SL' }, 'name')
    expect(SL).toBe('San Luis Potosí')
  })
  test('Sinaloa is equivalent to SI', () => {
    const SI = getProvince('MEX', { value: 'SI' }, 'name')
    expect(SI).toBe('Sinaloa')
  })
  test('Sonora is equivalent to SO', () => {
    const SO = getProvince('MEX', { value: 'SO' }, 'name')
    expect(SO).toBe('Sonora')
  })
  test('Tabasco is equivalent to TB', () => {
    const TB = getProvince('MEX', { value: 'TB' }, 'name')
    expect(TB).toBe('Tabasco')
  })
  test('Tamaulipas is equivalent to TM', () => {
    const TM = getProvince('MEX', { value: 'TM' }, 'name')
    expect(TM).toBe('Tamaulipas')
  })
  test('Tlaxcala is equivalent to TL', () => {
    const TL = getProvince('MEX', { value: 'TL' }, 'name')
    expect(TL).toBe('Tlaxcala')
  })
  test('Veracruz is equivalent to VE', () => {
    const VE = getProvince('MEX', { value: 'VE' }, 'name')
    expect(VE).toBe('Veracruz')
  })
  test('Yucatán is equivalent to YU', () => {
    const YU = getProvince('MEX', { value: 'YU' }, 'name')
    expect(YU).toBe('Yucatán')
  })
  test('Zacatecas is equivalent to ZA', () => {
    const ZA = getProvince('MEX', { value: 'ZA' }, 'name')
    expect(ZA).toBe('Zacatecas')
  })
})
test('Is getProvince value is default', () => {
  const province = getProvinces('')
  expect(province).toBeNull
})
test('Is getProvince value is default', () => {
  const province = getProvinces('ALL')
  expect(province).not.toBeNull
})
test('The country size must be 3', () => {
  const arrayLength = getCountries()
  expect(arrayLength.length).toEqual(3)
})

test('The region type size must be 4', () => {
  const arrayLength = getTypes()
  expect(arrayLength.length).toEqual(4)
})
