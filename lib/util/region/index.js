import provinces from './provinces.js'
import countries from './countries.js'
import types from './types.js'
import _ from 'lodash'

function getProvinces(code) {
  let options = []
  switch (code) {
    case 'CAN':
      options = provinces.canada
      break
    case 'USA':
      options = provinces.canada
      break
    case 'MEX':
      options = provinces.mexico
      break
    case 'ALL':
      options = _.flatten([provinces.canada, provinces.mexico])
      break
    default:
      options = []
  }

  // slice to create a new object reference
  return _.slice(options)
}

function getCountries() {
  return _.slice(countries)
}

function getTypes() {
  return _.slice(types)
}

/**
 * Find type by query
 *
 * @param {*} query sample query = {value: 'X'}
 * @param {string} field - optional. return one field
 */
function getType(query, field) {
  const foundType = _.find(types, query)

  if (field) {
    return _.get(foundType, field, '')
  }

  return foundType
}

/**
 * Find country by query
 * @param {*} query sample query = {value: 'CAN'}
 * @param {*} field optional
 */
function getCountry(query, field) {
  const foundCountry = _.find(countries, query)

  if (field) {
    return _.get(foundCountry, field, '')
  }

  return foundCountry
}

/**
 * Find province by country code and query
 * @param {*} countryCode sample = 'CAN'
 * @param {*} query { value: 'AB'}
 * @param {*} field optional. The field to return
 */
function getProvince(countryCode, query, field) {
  const provincesByCC = getProvinces(countryCode)
  const foundProvince = _.find(provincesByCC, query)

  if (field) {
    return _.get(foundProvince, field, '')
  }

  return foundProvince
}

export {
  getProvinces,
  getProvince,
  getCountries,
  getCountry,
  getTypes,
  getType
}
