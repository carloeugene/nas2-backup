import _ from 'lodash'

function ellipsis(length) {
  return (data, type, row) => {
    if (type === 'display') {
      return _.truncate(_.toString(data), { length: length || 20 })
    }

    return data
  }
}

function applyFilter(table, baseUrl, filter) {
  const aFilter = _.pickBy(filter, _.identity) // remove props with no value
  const param = $.param(aFilter)
  table.ajax.url(`${baseUrl}?${param}`).load()
}

export { ellipsis, applyFilter }
