const lists = {
  '/company': {
    title: 'Companies',
    breadcrumbsList: [
      {
        name: 'Home',
        link: '/'
      },
      {
        name: 'System Admin',
        link: '/'
      },
      {
        name: 'Companies',
        link: '/company'
      }
    ]
  },
  '/company/add': {
    title: 'Companies',
    breadcrumbsList: [
      {
        name: 'Home',
        link: '/'
      },
      {
        name: 'System Admin',
        link: '/'
      },
      {
        name: 'Companies',
        link: '/company'
      },
      {
        name: 'Add Company',
        link: '/company/Add'
      }
    ]
  },
  '/': {
    title: 'Home',
    breadcrumbsList: [
      {
        name: 'Home',
        link: '/'
      }
    ]
  },
  ___default___: {
    title: 'Home',
    breadcrumbsList: [
      {
        name: 'Home',
        link: '/'
      }
    ]
  }
}
export { lists }
