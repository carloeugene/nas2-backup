import { shallowMount } from '@vue/test-utils'
import Component from './index.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'
import NasInput from '~/components/form/Input.vue'
import NasTable from '~/components/table/es.vue'

describe('Company listings component', () => {
  function factory() {
    return shallowMount(Component, {
      localVue,
      i18n,
      store,
      router,
      stubs: {
        NasInput,
        NasTable
      },
      mocks: {
        $axios: {
          $post(url, filter) {
            return new Promise(resolve => {
              resolve({
                hits: {
                  total: 12,
                  hits: [
                    {
                      _source: {
                        app_contact_name: 'Mr. Tesla'
                      }
                    }
                  ]
                }
              })
            })
          }
        }
      }
    })
  }
  test('is Vue instance', () => {
    const wrapper = factory()

    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should have "ABC" client name', () => {
    const wrapper = factory()

    const contactName = wrapper.find('input#app_contact_name')
    contactName.setValue('ABC')
    const filter = wrapper.vm.$data.filter
    expect(filter.app_contact_name).toBe('ABC')
  })

  it('should clear client name', () => {
    const wrapper = factory()

    const contactName = wrapper.find('input#app_contact_name')
    const clearButton = wrapper.find('.btn.clear')
    contactName.setValue('ABC')
    expect(wrapper.vm.$data.filter.app_contact_name).toBe('ABC')
    clearButton.trigger('click')

    expect(wrapper.vm.$data.filter.app_contact_name).toBeFalsy()
  })

  it('should have country function', () => {
    const wrapper = factory()
    const countryFn = wrapper.vm.country()
    const country = countryFn('CAN')
    expect(typeof countryFn).toBe('function')
    expect(country).toBe('Canada')
  })

  it('should have province function', () => {
    const wrapper = factory()
    const provinceFn = wrapper.vm.province()
    const province = provinceFn('AB', { countryCode: 'CAN' })
    expect(typeof provinceFn).toBe('function')
    expect(province).toBe('Alberta')
  })

  it('should set date range', () => {
    const wrapper = factory()

    const dateFrom = wrapper.find('input#req_date_from')
    const dateTo = wrapper.find('input#req_date_to')
    const search = wrapper.find('.btn.search')
    dateFrom.setValue('2019-03-26')
    dateTo.setValue('2019-03-27')

    search.trigger('click')
    expect(wrapper.vm.$data.reqDateRange.from).toBe('2019-03-26')
    expect(wrapper.vm.$data.reqDateRange.to).toBe('2019-03-27')
  })
})
