import { shallowMount } from '@vue/test-utils'
import Component from './index.vue'

describe('Index Component', () => {
  test('is a Vue instance', () => {
    const wrapper = shallowMount(Component)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('title should be NAS 2', () => {
    const title = shallowMount(Component).find('.title')
    expect(title.text()).toBe('NAS 2')
  })

  test('subtitle should be NAS project', () => {
    const subtitle = shallowMount(Component).find('.subtitle')
    expect(subtitle.text()).toBe('NAS project')
  })
})
