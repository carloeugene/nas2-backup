import { shallowMount } from '@vue/test-utils'
import Component from './index.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Company listings component', () => {
  test('is Vue instance', () => {
    const wrapper = shallowMount(Component, {
      localVue,
      i18n,
      store,
      router,
      stubs
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
