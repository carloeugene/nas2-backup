import { shallowMount } from '@vue/test-utils'
import Component from './index.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Company listings component', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs
    })

    return wrapper
  }

  it('is Vue instance', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should have "Welcome to NAS" title', () => {
    const wrapper = factory()
    expect(wrapper.find('.h1').text()).toBe('Welcome to NAS')
  })

  it('should have "FR-Welcome to NAS" title', () => {
    const wrapper = factory()
    const localSelect = wrapper.find('.local-select')
    localSelect.setValue('fr')
    expect(wrapper.find('.h1').text()).toBe('FR-Welcome to NAS')
  })
})
