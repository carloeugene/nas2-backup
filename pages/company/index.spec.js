import { shallowMount } from '@vue/test-utils'
import AddCompany from './add.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'

describe('Add New Company', () => {
  test('is button clicked', () => {
    const wrapper = shallowMount(AddCompany, {
      localVue,
      i18n,
      store,
      router,
      stubs
    })

    wrapper.find('button').trigger('click')
    expect(wrapper).toBeTruthy()
  })
})
