jest.mock('sweetalert2')
import { shallowMount } from '@vue/test-utils'
import Component from './add.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'
import Swal from 'sweetalert2'

describe('Add territory', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs
    })

    return wrapper
  }

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Swal.mockClear()
  })

  it('should be a component', () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
