jest.mock('sweetalert2')
import { shallowMount } from '@vue/test-utils'
import Component from './_id.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'
import Swal from 'sweetalert2'

describe('Input compnent', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs
    })

    return wrapper
  }

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Swal.mockClear()
  })

  it('should have View Regions button', () => {
    const wrapper = factory()
    const viewButton = wrapper.find('.btn.view-region')
    expect(viewButton.text()).toBe('View Regions')
  })

  it('should fire a success', async () => {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs,
      mocks: {
        $axios: {
          $put: () =>
            new Promise(resolve => {
              resolve({
                name: 'update region'
              })
            })
        }
      }
    })

    await wrapper.vm.onSubmit({
      name: 'Region name'
    })

    expect(Swal.fire).toHaveBeenCalled()
    expect(Swal.fire).toHaveBeenCalledWith(
      'Success!',
      'Data Successfully Updated',
      'success'
    )
  })

  it('should fire an error', async () => {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs,
      mocks: {
        $axios: {
          $put: () => new Promise((resolve, reject) => reject())
        }
      }
    })

    await wrapper.vm.onSubmit({
      name: 'Region name'
    })

    expect(Swal.fire).toHaveBeenCalled()
    expect(Swal.fire).toHaveBeenCalledWith(
      'Error!',
      'Error on Updating Data',
      'error'
    )
  })
})
