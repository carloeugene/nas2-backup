jest.mock('sweetalert2')
import { shallowMount } from '@vue/test-utils'
import Component from './add.vue'
import { localVue, store, i18n, router, stubs } from '~/test-utils'
import Swal from 'sweetalert2'

describe('Group page component', () => {
  function factory() {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs
    })

    return wrapper
  }

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Swal.mockClear()
  })

  it('should have View Group button', () => {
    const wrapper = factory()
    const viewButton = wrapper.find('.btn.view-group')
    expect(viewButton.text()).toBe('View Groups')
  })

  it('should fire a success', async () => {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs,
      mocks: {
        $axios: {
          $post: () =>
            new Promise(resolve => {
              resolve({
                name: 'add group'
              })
            })
        }
      }
    })

    await wrapper.vm.onSubmitGroup({
      name: 'group name'
    })

    expect(Swal.fire).toHaveBeenCalled()
    expect(Swal.fire).toHaveBeenCalledWith(
      'Success!',
      'New Data Successfully Added',
      'success'
    )
  })

  it('should fire an error', async () => {
    const wrapper = shallowMount(Component, {
      store,
      i18n,
      localVue,
      router,
      stubs,
      mocks: {
        $axios: {
          $post: () => new Promise((resolve, reject) => reject())
        }
      }
    })

    await wrapper.vm.onSubmitGroup({
      name: 'group name'
    })

    expect(Swal.fire).toHaveBeenCalled()
    expect(Swal.fire).toHaveBeenCalledWith(
      'Error!',
      'Error on Adding New Data',
      'error'
    )
  })
})
