# nas2-front-end

> Nas project

## Build Setup

``` bash

# instruction for running this project locally
# the machine should be connected to our vpn server
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
# to connect to your local machine to uat backend api
$ npm run uat


#######################################################
# this is for production build only
# build for production and launch server
$ npm run build
$ npm start
# generate static project
$ npm run generate

```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## feature branching
``` bash

# checkout master
git checkout master

# update master branch
git pull origin master

# create your feature branch base into master where <number> is JIRA ticket number
git checkout -b feature/NAS-<number>

```

## running nuxt using local api
start using local api
1. Make sure mysql running
  `docker ps`
  If no mysql is running, check all the containers
    `docker start nas-db`

Open eclipse, click the debug icon arrow down then click API debug button. To check if running go to http://localhost:8080/swagger-ui.html

Run nas2-front-end using local api `npm run local`
