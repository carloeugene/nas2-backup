FROM node:latest

WORKDIR /app

ARG NODE_ENV

ENV NODE_ENV $NODE_ENV

COPY package.json /app

RUN npm install

COPY . /app

RUN npm run build

EXPOSE 3000

ENV HOST 0.0.0.0

ENTRYPOINT [ "npm", "run", "start-nuxt" ]
